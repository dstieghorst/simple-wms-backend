package info.stieghorst.simpleWMS.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

/*
 * picking order
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
@Entity
@ToString(exclude="items")
public class PickingOrder {
	
	public static final String STATUS_NEW = "NEU";
	public static final String STATUS_IN_PROGRESS = "IN ARBEIT";
	public static final String STATUS_FINISHED = "ERLEDIGT";
	
	// fields

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String orderName;
	
	private String orderStatus;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private User user;
	
	@Temporal(TemporalType.TIMESTAMP)
    private Date created;

	@Temporal(TemporalType.TIMESTAMP)
    private Date lastChanged;
	
	@OneToMany(mappedBy="order", fetch=FetchType.LAZY)
	@JsonIgnore
	private List<PickingItem> items;

	public PickingOrder() {
		this.created = new Date();
	}
	
	public PickingOrder(String orderName) {
		this.orderName = orderName;
		this.orderStatus = STATUS_NEW;
		this.created = new Date();
		this.items = new ArrayList<>();
	}
	
	public void addItem(PickingItem item) {
		if(items == null) {
			items = new ArrayList<>();
		}
		items.add(item);
	}

}
