package info.stieghorst.simpleWMS.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.stieghorst.simpleWMS.domain.Location;
import info.stieghorst.simpleWMS.repository.LocationRepository;
import lombok.extern.slf4j.Slf4j;

/*
 * REST controller for all location related requests
 */
@RestController
@Slf4j
public class LocationController {

	private final LocationRepository repository;

	private final Integer DEFAULT_PAGE_SIZE = 5;

	private Sort standardSort = new Sort(Sort.Direction.ASC, "aisle").and(new Sort(Sort.Direction.ASC, "stack"))
			.and(new Sort(Sort.Direction.ASC, "level"));

	public LocationController(LocationRepository repository) {
		this.repository = repository;
	}

	/*
	 * GET locations
	 * search criteria not required
	 */
	@GetMapping("/locations")
	List<Location> getLocations(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "locked", required = false) String locked,
			@RequestParam(value = "noStock", required = false) Boolean noStock,
			@RequestParam(value = "aisle", required = false) Integer aisle,
			@RequestParam(value = "stack", required = false) Integer stack
	) {

		log.debug("getLocations");
		
		List<Location> locationList = new ArrayList<>();

		// translate request params
		boolean lockFlagRelevant = false;
		boolean searchLocked = false;
		if(locked == null) {
			locked = "egal";
		}
		String lockedString = locked.trim().toLowerCase();
		log.debug("lockedString = {}", lockedString);
		switch (lockedString) {
		case "ja":
			lockFlagRelevant = true;
			searchLocked = true;
			break;
		case "nein":
			lockFlagRelevant = true;
			searchLocked = false;
			break;
		case "egal":
			lockFlagRelevant = false;
			break;
		}
		log.debug("lockFlagRelevant: {}, searchLocked: {}", lockFlagRelevant, searchLocked);
		
		// case 1 - filter by id
		if (id != null) { 
			log.debug("search for id {}", id);
			List<Long> ids = new ArrayList<>();
			ids.add(id);
			repository.findAllById(ids).forEach(locationList::add);
		}
		// case 2 - filter by lockFlag and existing stock
		else if(lockFlagRelevant && noStock != null && noStock == true) { 
			log.debug("filter by lockFlag and existing Stock");
			locationList = repository.findByLockedAndStockIsNull(searchLocked);
		}
		// case 3 - filter by lockFlag only
		else if(lockFlagRelevant && (noStock == null || noStock == false)) { 
			log.debug("filter by lockFlag only");
			locationList = repository.findByLocked(searchLocked, standardSort);
		}
		// case 4 - no stock
		else if(noStock != null && noStock == true) { 
			log.debug("locations with no stock");
			locationList = repository.findByStockIsNull();
		}
		// case 5 - filter by aisle and stack
		else if(aisle != null && aisle > 0 && stack != null && stack > 0) {
			log.debug("filter by aisle {} and stack {}", aisle, stack);
			locationList = repository.findByAisleAndStack(aisle, stack, standardSort);
		}
		// case 6 - filter by aisle
		else if(aisle != null && aisle > 0) {
			log.debug("filter by aisle {}", aisle);
			locationList = repository.findByAisle(aisle, standardSort);
		}
		// case 7 - no filter
		else {
			log.debug("no filter - get all locations");
			repository.findAll().iterator().forEachRemaining(locationList::add);
		}
		
		return locationList;
	}

	/*
	 * GET locations
	 * search criteria not required
	 * 
	 * - version for access by page
	 *   prefix "/paged" in URL
	 */
	@GetMapping("/paged/locations")
	Page<Location> getLocationsPage(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "locked", required = false) String locked,
			@RequestParam(value = "noStock", required = false) Boolean noStock,
			@RequestParam(value = "aisle", required = false) Integer aisle,
			@RequestParam(value = "stack", required = false) Integer stack,
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize
	) {

		log.debug("getLocationsPage");
		
		PageRequest pageRequest = null;
		if(pageNumber == null) {
			pageNumber = 0;
		}
		if (pageSize == null || pageSize < 0) {
			pageSize = DEFAULT_PAGE_SIZE;
		}
		pageRequest = PageRequest.of(pageNumber, pageSize);
		log.debug("pageNumber {}, pageSize {}", pageNumber, pageSize);


		Page<Location> page = null;

		boolean lockFlagRelevant = false;
		boolean searchLocked = false;
		if(locked == null) {
			locked = "egal";
		}
		String lockedString = locked.trim().toLowerCase();
		log.debug("lockedString = {}", lockedString);
		switch (lockedString) {
		case "ja":
			lockFlagRelevant = true;
			searchLocked = true;
			break;
		case "nein":
			lockFlagRelevant = true;
			searchLocked = false;
			break;
		case "egal":
			lockFlagRelevant = false;
			break;
		}
		log.debug("lockFlagRelevant: {}, searchLocked: {}", lockFlagRelevant, searchLocked);
		
		// case 1 - filter by id
		if (id != null) { 
			log.debug("search for id {}", id);
			page = repository.findAllById(id, pageRequest);
		}
		// case 2 - filter by lockFlag and existing stock
		else if(lockFlagRelevant && noStock != null && noStock == true) { 
			log.debug("filter by lockFlag and existing Stock");
			page = repository.findByLockedAndStockIsNull(searchLocked, pageRequest);
		}
		// case 3 - filter by lockFlag only
		else if(lockFlagRelevant && (noStock == null || noStock == false)) { 
			log.debug("filter by lockFlag only");
			page = repository.findByLocked(searchLocked, pageRequest);
		}
		// case 4 - no stock
		else if(noStock != null && noStock == true) { 
			log.debug("locations with no stock");
			page = repository.findByStockIsNull(pageRequest);
		}
		// case 5 - filter by aisle and stack
		else if(aisle != null && aisle > 0 && stack != null && stack > 0) {
			log.debug("filter by aisle {} and stack {}", aisle, stack);
			page = repository.findByAisleAndStack(aisle, stack, pageRequest);
		}
		// case 6 - filter by aisle
		else if(aisle != null && aisle > 0) {
			log.debug("filter by aisle {}", aisle);
			page = repository.findByAisle(aisle, pageRequest);
		}
		// case 7 - no filter
		else {
			log.debug("no filter - get all locations");
			page = repository.findAll(pageRequest);
		}
		
		return page;
	}

	/*
	 * POST new location
	 */
	@PostMapping("/locations")
	Location newLocation(@RequestBody Location newLocation) {
		return repository.save(newLocation);
	}

	/*
	 * GET single location
	 */
	@GetMapping("/locations/{id}")
	Location getLocationById(@PathVariable Long id) {
		return repository.findById(id).orElseThrow(() -> new LocationNotFoundException(id));
	}

	/*
	 * PUT existing location
	 */
	@PutMapping("/locations/{id}")
	Location updateLocation(@RequestBody Location updatedLocation, @PathVariable Long id) {
		Optional<Location> locationOptional = repository.findById(id);
		if (locationOptional.isPresent()) {
			Location location = locationOptional.get();
			location.setLocked(updatedLocation.getLocked());
			return repository.save(location);
		} else {
			throw new LocationNotFoundException(id);
		}
	}

	/*
	 * DELETE location
	 */
	@DeleteMapping("/locations/{id}")
	void deleteLocation(@PathVariable Long id) {
		repository.deleteById(id);
	}

}
