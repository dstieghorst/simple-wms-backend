package info.stieghorst.simpleWMS.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.PagingAndSortingRepository;

import info.stieghorst.simpleWMS.domain.Stock;

/*
 * make use of Spring PagingAndSortingRepository for stock CRUD operations, with pagination option
 */
public interface StockRepository extends PagingAndSortingRepository<Stock, Long>{
	
	// all
	
	Page<Stock> findAll(Pageable pageable);
	
	// id

	Page<Stock> findAllById(List<Long> ids, Pageable pageable);

	// productNo
	
	List<Stock> findByProductProductNoContaining(String productNo, Sort sort);
	
	Page<Stock> findByProductProductNoContaining(String productNoWildcards, Pageable pageable);
	
	// loc == null

	List<Stock> findByLocIsNull(Sort sort);
	
	Page<Stock> findByLocIsNull(Pageable pageable);
	
	// productId

	List<Stock> findByProductId(Long id, Sort sort);
	
	Page<Stock> findByProductId(Long productId, Pageable pageable);

	// locId
	
	List<Stock> findByLocId(Long id, Sort sort);
	
	Page<Stock> findByLocId(Long locationId, Pageable pageable);
	
	// loc.aisle

	List<Stock> findByLocAisle(Integer aisle, Sort sort);
	
	Page<Stock> findByLocAisle(Integer aisle, Pageable pageable);
	
	// loc.aisle/stack

	List<Stock> findByLocAisleAndLocStack(Integer aisle, Integer stack, Sort sort);
	
	Page<Stock> findByLocAisleAndLocStack(Integer aisle, Integer stack, Pageable pageable);
	
	// loc.aisle/stack/level

	List<Stock> findByLocAisleAndLocStackAndLocLevel(Integer aisle, Integer stack, Integer level, Sort sort);

	Page<Stock> findByLocAisleAndLocStackAndLocLevel(Integer aisle, Integer stack, Integer level, Pageable pageable);

}
