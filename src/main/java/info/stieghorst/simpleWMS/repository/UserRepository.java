package info.stieghorst.simpleWMS.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.domain.User;

/*
 * make use of Spring JpaRepository for user related CRUD operations
 */
public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByUsername(String username);
	
	@Query("From User u where :role member u.roles")
	List<User> findByRole(@Param("role") Role role);
}
