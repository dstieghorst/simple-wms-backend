package info.stieghorst.simpleWMS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import info.stieghorst.simpleWMS.domain.Role;

/*
 * make use of Spring JpaRepository for role related CRUD operations
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	Role findByRoleName(String roleName);

}
