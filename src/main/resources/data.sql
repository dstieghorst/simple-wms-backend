INSERT INTO `role` (`id`, `role_name`) VALUES
	(1, 'STANDARD');
INSERT INTO `role` (`id`, `role_name`) VALUES
	(2, 'ADMIN');

INSERT INTO `user` (`id`, `account_expired`, `account_locked`, `credentials_expired`, `enabled`, `password`, `username`) VALUES
	(1, b'0', b'0', b'0', b'0', '$2a$11$MR25efxR5BXUwAVs3tH4deYuHh6LzrIQQ1oLg430s3mlYvyiHPx9S', 'tester');
INSERT INTO `user` (`id`, `account_expired`, `account_locked`, `credentials_expired`, `enabled`, `password`, `username`) VALUES
	(2, b'0', b'0', b'0', b'0', '$2a$11$MR25efxR5BXUwAVs3tH4deYuHh6LzrIQQ1oLg430s3mlYvyiHPx9S', 'admin');

INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
	(1, 1);
INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
	(2, 1);
INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
	(2, 2);

INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(1, b'1', 'Testartikel A8A4M1', 'A8A4M1', 'A8A4M1');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(2, b'1', 'Testartikel H1P8W3', 'H1P8W3', 'H1P8W3');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(3, b'1', 'Testartikel N1H0H0', 'N1H0H0', 'N1H0H0');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(4, b'0', 'Testartikel I1F6B2', 'I1F6B2', 'I1F6B2');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(5, b'1', 'Testartikel Y2O4L9', 'Y2O4L9', 'Y2O4L9');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(6, b'1', 'Testartikel G8O1X9', 'G8O1X9', 'G8O1X9');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(7, b'1', 'Testartikel O3P6G4', 'O3P6G4', 'O3P6G4');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(8, b'0', 'Testartikel T1L0P7', 'T1L0P7', 'T1L0P7');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(9, b'1', 'Testartikel D3H2Q7', 'D3H2Q7', 'D3H2Q7');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(10, b'1', 'Testartikel D3U0S5', 'D3U0S5', 'D3U0S5');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(11, b'1', 'Testartikel T4S6N6', 'T4S6N6', 'T4S6N6');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(12, b'0', 'Testartikel L6H9U7', 'L6H9U7', 'L6H9U7');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(13, b'1', 'Testartikel M2G8R7', 'M2G8R7', 'M2G8R7');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(14, b'1', 'Testartikel P9Y9M6', 'P9Y9M6', 'P9Y9M6');
INSERT INTO `product` (`id`, `active`, `description`, `name`, `product_no`) VALUES
	(15, b'1', 'Testartikel B1C6X2', 'B1C6X2', 'B1C6X2');

INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(1, 1, 1, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(2, 1, 2, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(3, 1, 3, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(4, 1, 1, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(5, 1, 2, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(6, 1, 3, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(7, 1, 1, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(8, 1, 2, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(9, 1, 3, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(10, 1, 1, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(11, 1, 2, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(12, 1, 3, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(13, 1, 1, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(14, 1, 2, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(15, 1, 3, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(16, 2, 1, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(17, 2, 2, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(18, 2, 3, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(19, 2, 1, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(20, 2, 2, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(21, 2, 3, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(22, 2, 1, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(23, 2, 2, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(24, 2, 3, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(25, 2, 1, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(26, 2, 2, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(27, 2, 3, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(28, 2, 1, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(29, 2, 2, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(30, 2, 3, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(31, 3, 1, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(32, 3, 2, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(33, 3, 3, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(34, 3, 1, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(35, 3, 2, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(36, 3, 3, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(37, 3, 1, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(38, 3, 2, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(39, 3, 3, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(40, 3, 1, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(41, 3, 2, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(42, 3, 3, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(43, 3, 1, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(44, 3, 2, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(45, 3, 3, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(46, 4, 1, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(47, 4, 2, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(48, 4, 3, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(49, 4, 1, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(50, 4, 2, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(51, 4, 3, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(52, 4, 1, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(53, 4, 2, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(54, 4, 3, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(55, 4, 1, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(56, 4, 2, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(57, 4, 3, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(58, 4, 1, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(59, 4, 2, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(60, 4, 3, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(61, 5, 1, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(62, 5, 2, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(63, 5, 3, b'0', 1);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(64, 5, 1, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(65, 5, 2, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(66, 5, 3, b'0', 2);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(67, 5, 1, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(68, 5, 2, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(69, 5, 3, b'0', 3);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(70, 5, 1, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(71, 5, 2, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(72, 5, 3, b'0', 4);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(73, 5, 1, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(74, 5, 2, b'0', 5);
INSERT INTO `location` (`id`, `aisle`, `level`, `locked`, `stack`) VALUES
	(75, 5, 3, b'0', 5);

	
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(1, 322, 19, 12);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(2, 498, 2, 15);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(3, 102, 49, 13);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(4, 318, 65, 6);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(5, 424, 6, 11);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(6, 243, 52, 3);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(7, 35, 17, 3);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(8, 367, 22, 6);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(9, 54, 8, 4);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(10, 411, 75, 5);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(11, 78, 23, 7);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(12, 371, 38, 10);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(13, 463, 45, 13);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(14, 354, 11, 12);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(15, 98, 69, 10);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(16, 280, 59, 2);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(17, 343, 27, 7);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(18, 270, 25, 9);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(19, 145, 21, 6);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(20, 95, 48, 2);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(21, 37, 30, 4);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(22, 170, 61, 3);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(23, 488, 10, 9);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(24, 257, 71, 14);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(25, 475, 41, 9);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(26, 76, 7, 5);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(27, 82, 57, 6);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(28, 90, 3, 3);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(29, 249, 63, 8);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(30, 13, 40, 2);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(31, 329, 15, 3);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(32, 178, 24, 7);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(33, 457, 53, 8);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(34, 123, 34, 2);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(35, 249, 46, 12);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(36, 301, 13, 7);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(37, 230, 12, 14);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(38, 56, 31, 6);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(39, 29, 68, 11);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(40, 144, 73, 14);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(41, 37, 36, 10);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(42, 369, 4, 3);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(43, 399, 58, 1);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(44, 230, 44, 4);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(45, 383, 16, 12);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(46, 358, 42, 10);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(47, 338, 33, 4);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(48, 12, 70, 1);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(49, 441, 28, 15);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(50, 103, 5, 2);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(51, 313, 1, 8);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(52, 142, 26, 2);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(53, 494, 37, 2);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(54, 418, 39, 6);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(55, 35, 67, 3);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(56, 369, 56, 7);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(57, 287, 64, 13);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(58, 282, 32, 12);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(59, 270, 35, 13);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(60, 455, 72, 6);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(61, 146, 18, 11);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(62, 440, 51, 8);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(63, 482, 60, 9);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(64, 73, 47, 7);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(65, 226, 9, 5);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(66, 339, 74, 8);
INSERT INTO `stock` (`id`, `units`, `loc_id`, `product_id`) VALUES
	(67, 304, 55, 15);
	
INSERT INTO `picking_order` (`id`, `created`, `last_changed`, `order_name`, `order_status`, `user_id`) VALUES
	(1, '2018-11-15 08:08:45', NULL, 'Testbestellung 1', 'IN ARBEIT', 2);
INSERT INTO `picking_order` (`id`, `created`, `last_changed`, `order_name`, `order_status`, `user_id`) VALUES
	(2, '2018-11-15 08:08:45', NULL, 'Testbestellung 2', 'IN ARBEIT', 1);
INSERT INTO `picking_order` (`id`, `created`, `last_changed`, `order_name`, `order_status`, `user_id`) VALUES
	(3, '2018-11-15 08:08:45', NULL, 'Testbestellung 3', 'NEU', NULL);
INSERT INTO `picking_order` (`id`, `created`, `last_changed`, `order_name`, `order_status`, `user_id`) VALUES
	(4, '2018-11-15 08:08:45', NULL, 'Testbestellung 4', 'NEU', NULL);
INSERT INTO `picking_order` (`id`, `created`, `last_changed`, `order_name`, `order_status`, `user_id`) VALUES
	(5, '2018-11-15 08:08:45', NULL, 'Testbestellung 5', 'NEU', NULL);
	
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(1, 0, 'OFFEN', 4, 1, 9);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(2, 0, 'OFFEN', 5, 1, 10);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(3, 0, 'OFFEN', 20, 1, 4);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(4, 0, 'OFFEN', 1, 1, 10);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(5, 0, 'OFFEN', 17, 2, 11);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(6, 0, 'OFFEN', 7, 2, 11);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(7, 0, 'OFFEN', 7, 2, 9);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(8, 0, 'OFFEN', 16, 2, 15);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(9, 0, 'OFFEN', 15, 2, 1);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(10, 0, 'OFFEN', 15, 2, 9);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(11, 0, 'OFFEN', 18, 2, 7);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(12, 0, 'OFFEN', 2, 2, 15);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(13, 0, 'OFFEN', 10, 2, 3);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(14, 0, 'OFFEN', 5, 2, 8);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(15, 0, 'OFFEN', 3, 3, 1);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(16, 0, 'OFFEN', 2, 3, 12);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(17, 0, 'OFFEN', 3, 3, 12);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(18, 0, 'OFFEN', 9, 3, 13);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(19, 0, 'OFFEN', 12, 4, 1);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(20, 0, 'OFFEN', 18, 4, 7);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(21, 0, 'OFFEN', 14, 5, 1);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(22, 0, 'OFFEN', 13, 5, 4);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(23, 0, 'OFFEN', 8, 5, 6);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(24, 0, 'OFFEN', 18, 5, 11);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(25, 0, 'OFFEN', 16, 5, 8);
INSERT INTO `picking_item` (`id`, `actual_quantity`, `item_status`, `order_quantity`, `order_id`, `product_id`) VALUES
	(26, 0, 'OFFEN', 9, 5, 3);
	
