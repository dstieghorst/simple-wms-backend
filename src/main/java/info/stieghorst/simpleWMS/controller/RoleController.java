package info.stieghorst.simpleWMS.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;

/*
 * REST controller for all role related requests
 */
@RestController
@Slf4j
public class RoleController {
	
	private final RoleRepository repository;
	
	public RoleController(RoleRepository repository) {
		this.repository = repository;
	}
	
	/*
	 * GET list of all existing roles
	 */
	@GetMapping("/roles")
	List<Role> allRoles() {
		log.debug("RoleController.allRoles - START");
		
		return repository.findAll();
	}
	
	/*
	 * POST a new role
	 */
	@PostMapping("/roles")
	Role newRole(@RequestBody Role newRole) {
		log.debug("RoleController.newRole - START");
		log.debug("RoleController.newRole - {}", newRole);
		
		return repository.save(newRole);
	}
	
	/*
	 * GET a single role
	 */
	@GetMapping("/roles/{id}")
	Role getRoleById(@PathVariable Long id) {
		log.debug("RoleController.getRoleById - START {}", id);
		
		return repository.findById(id).orElseThrow(() -> new RoleNotFoundException(id));
	}
	
	/*
	 * PUT an existing role
	 */
	@PutMapping("/roles/{id}")
	Role updateRole(@RequestBody Role updatedRole, @PathVariable Long id) {
		log.debug("RoleController.updateRole - START {}", id);
		log.debug("RoleController.updateRole - updatedRole {}", updatedRole);
		
		Optional<Role> roleOptional = repository.findById(id);
		if(roleOptional.isPresent()) {
			Role role = roleOptional.get();
			log.debug("RoleController.updateRole - existing role {}", role);
			role.setRoleName(updatedRole.getRoleName());
			return repository.save(role);
		}
		else {
			throw new RoleNotFoundException(id);
		}
	}
	
	/*
	 * DELETE a role
	 */
	@DeleteMapping("/roles/{id}")
	void deleteRole(@PathVariable Long id) {
		log.debug("RoleController.deleteRole - START {}", id);
		
		repository.deleteById(id);
	}

}
