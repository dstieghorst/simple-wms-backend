package info.stieghorst.simpleWMS.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.stieghorst.simpleWMS.domain.PickingItem;
import info.stieghorst.simpleWMS.domain.PickingOrder;
import info.stieghorst.simpleWMS.repository.PickingItemRepository;
import info.stieghorst.simpleWMS.repository.PickingOrderRepository;
import lombok.extern.slf4j.Slf4j;

/*
 * REST controller for all picking item related requests
 */
@RestController
@Slf4j
public class PickingItemController {
	
	private final PickingItemRepository repository;
	
	private final PickingOrderRepository orderRepository;
	
	private Sort itemSort = new Sort(Sort.Direction.ASC, "id");
	
	public PickingItemController(PickingItemRepository repository, PickingOrderRepository orderRepository) {
		this.repository = repository;
		this.orderRepository = orderRepository;
	}
	
	/*
	 * GET list of picking items
	 * search criteria not required
	 */
	@GetMapping("/items")
	List<PickingItem> getPickingItems(@RequestParam(value = "orderId", required = false) Long orderId) {
		
		log.debug("getPickingItems");
		
		List<PickingItem> itemList = new ArrayList<>();
		
		if(orderId != null) {
			log.debug("search for orderId {}", orderId);
			itemList = repository.findByOrderId(orderId, itemSort);
		}
		else {
			log.debug("get all PickingItems");
			itemList = repository.findAll();
		}

		return itemList;
	}
	
	/*
	 * POST new picking items
	 */
	@PostMapping("/items")
	PickingItem newPickingItem(@RequestBody PickingItem newItem) {
		log.debug("newPickingItem");
		log.debug("item {}", newItem);
		
		if(newItem.getOrder() != null) {
			Long orderId = newItem.getOrder().getId();
			Optional<PickingOrder> orderOptional = orderRepository.findById(orderId);
			if(orderOptional.isPresent()) {
				PickingOrder order = orderOptional.get();
				order.addItem(newItem);
				orderRepository.save(order);
				newItem.setOrder(order);
				return repository.save(newItem);
			}
			else  {
				log.error("orderId not found! {}", orderId);
				throw new PickingOrderNotFoundException(orderId);
			}
		}
		else {
			log.error("no picking order referenced in item!");
			throw new PickingOrderNotFoundException(new Long(-1));
		}
	}
	
	/*
	 * GET a single picking item
	 */
	@GetMapping("/items/{id}")
	PickingItem getPickingItemById(@PathVariable Long id) {
		return repository.findById(id).orElseThrow(() -> new PickingItemNotFoundException(id));
	}
	
	/*
	 * PUT an existing picking item
	 */
	@PutMapping("/items/{id}")
	PickingItem updatePickingItem(@RequestBody PickingItem updatedItem, @PathVariable Long id) {
		Optional<PickingItem> itemOptional = repository.findById(id);
		if(itemOptional.isPresent()) {
			PickingItem item = itemOptional.get();
			item.setProduct(updatedItem.getProduct());
			item.setItemStatus(updatedItem.getItemStatus());
			item.setOrderQuantity(updatedItem.getOrderQuantity());
			item.setActualQuantity(updatedItem.getActualQuantity());
			return repository.save(item);
		}
		else {
			throw new PickingItemNotFoundException(id);
		}
	}
	
	/*
	 * DELETE an existing picking item
	 */
	@DeleteMapping("/items/{id}")
	void deletePickingItem(@PathVariable Long id) {
		repository.deleteById(id);
	}
	
}
