package info.stieghorst.simpleWMS.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import info.stieghorst.simpleWMS.domain.PickingItem;
import info.stieghorst.simpleWMS.domain.PickingOrder;

/*
 * make use of Spring PagingAndSortingRepository for picking related CRUD operations, with pagination option
 */
public interface PickingOrderRepository extends PagingAndSortingRepository<PickingOrder, Long> {
	
	// id
	
	public Page<PickingOrder> findAllById(List<Long> ids, Pageable pageable);
	
	// order status

	public List<PickingOrder> findByOrderStatus(String orderStatus);
	
	public Page<PickingOrder> findByOrderStatus(String orderStatus, Pageable pageable);

	// created (date)
	
	public List<PickingOrder> findByCreatedBetween(Date dayBegin, Date dayEnd);

	public Page<PickingOrder> findByCreatedBetween(Date dayBegin, Date dayEnd, Pageable pageable);

	// username
	
	public List<PickingOrder> findByUserUsername(String username);
	
	public Page<PickingOrder> findByUserUsername(String username, Pageable pageable);

	public List<PickingOrder> findByUserUsernameNot(String username);
	
	public Page<PickingOrder> findByUserUsernameNot(String username, Pageable pageable);

	// no user assigned
	
	public List<PickingOrder> findByUserIsNull();

	public Page<PickingOrder> findByUserIsNull(Pageable pageable);
	
	// picking item in items list
	
	@Query("From PickingOrder o where :item member o.items")
	public List<PickingOrder> findByPickingItem(@Param("item")PickingItem item);

}
