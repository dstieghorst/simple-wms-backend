package info.stieghorst.simpleWMS.controller;

/*
 * exception: picking order not found
 */
public class PickingOrderNotFoundException extends RuntimeException {
	
	PickingOrderNotFoundException(Long id) {
		super("Could not find stock with id: " + id);
	}

}
