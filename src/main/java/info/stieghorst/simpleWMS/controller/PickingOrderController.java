package info.stieghorst.simpleWMS.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.stieghorst.simpleWMS.domain.PickingItem;
import info.stieghorst.simpleWMS.domain.PickingOrder;
import info.stieghorst.simpleWMS.repository.PickingItemRepository;
import info.stieghorst.simpleWMS.repository.PickingOrderRepository;
import lombok.extern.slf4j.Slf4j;

/*
 * REST controller for all picking order related requests
 */
@RestController
@Slf4j
public class PickingOrderController {
	
	private final PickingOrderRepository repository;
	
	private final PickingItemRepository itemRepository;
	
	private final Integer DEFAULT_PAGE_SIZE = 5;

	public PickingOrderController(PickingOrderRepository repository, PickingItemRepository itemRepository) {
		this.repository = repository;
		this.itemRepository = itemRepository;
	}
	
	/* 
	 * GET list of picking order
	 * search criteria not required
	 */
	@GetMapping("/orders")
	public List<PickingOrder> getPickingOrders(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "created", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date created,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "assigned", required = false) String assigned,
			@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "itemId", required = false) Long itemId
			) {
		
		log.debug("getPickingOrders");
		
		List<PickingOrder> orderList = new ArrayList<>();
		// case 1 - filter by id
		if (id != null) { 
			log.debug("search for id {}", id);
			List<Long> ids = new ArrayList<>();
			ids.add(id);
			repository.findAllById(ids).forEach(orderList::add);
		}
		// case 2 - filter by creationDate
		else if(created != null) {
			log.debug("search for created {}", created.toString());

			SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createdString = inputFormat.format(created);
			String beginString = createdString + " 00:00:00";
			String endString = createdString + " 23:59:59";
			Date dayBegin = created;
			Date dayEnd = created;
			try {
				dayBegin = outputFormat.parse(beginString);
				dayEnd = outputFormat.parse(endString);
			} catch (ParseException e) {
				log.error(e.getMessage());
			}
			log.debug("search orders created between {} and {}", dayBegin, dayEnd);
			orderList = repository.findByCreatedBetween(dayBegin, dayEnd);
		}
		// case 3 - filter by status
		else if(status != null && status.trim().length() > 0) {
			log.debug("search for status {}", status.trim());
			orderList = repository.findByOrderStatus(status.trim());
		}
		// case 4 - filter by user assignment
		else if(assigned != null && assigned.trim().length() > 0) {
			String assignmentString = assigned.trim().toLowerCase();
			String userNameString = "";
			
			log.debug("search for assigned {}", assignmentString);
			
			boolean usernameGiven = false;
			if(username != null && username.trim().length() > 0) {
				log.debug("username {}", username);
				usernameGiven = true;
				userNameString = username.trim();
			}
			
			if(assignmentString.equals("nobody")) {
				log.debug("search orders not yet assigned");
				orderList = repository.findByUserIsNull();
			}
			else if(usernameGiven && assignmentString.equals("own")) {
				log.debug("search order assigned to {}", userNameString);
				orderList = repository.findByUserUsername(userNameString);
			}
			else if(usernameGiven && assignmentString.endsWith("others")) {
				log.debug("search order not assigned to {}", userNameString);
				orderList = repository.findByUserUsernameNot(userNameString);
			}
			else {
				log.debug(" get all orders");
				repository.findAll().iterator().forEachRemaining(orderList::add);
			}
		}
		// case 5 - find orders for itemId
		else if(itemId != null) {
			PickingItem item = itemRepository.getOne(itemId);
			orderList = repository.findByPickingItem(item);
		}
		// case 6 - no filter
		else {
			log.debug("no filter - get all orders");
			repository.findAll().iterator().forEachRemaining(orderList::add);
		}

		return orderList;
	}
	
	/* 
	 * GET list of picking order
	 * search criteria not required
	 * 
	 * - version for access by page
	 *   prefix "/paged" in URL
	 */
	@GetMapping("/paged/orders")
	public Page<PickingOrder> getPickingOrdersPage(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "created", required = false) @DateTimeFormat(pattern="yyyy-MM-dd") Date created,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "assigned", required = false) String assigned,
			@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize
			) {
		
		log.debug("getPickingOrdersPage");
		
		PageRequest pageRequest = null;
		if(pageNumber == null) {
			pageNumber = 0;
		}
		if (pageSize == null || pageSize < 0) {
			pageSize = DEFAULT_PAGE_SIZE;
		}
		pageRequest = PageRequest.of(pageNumber, pageSize);
		log.debug("pageNumber {}, pageSize {}", pageNumber, pageSize);


		Page<PickingOrder> page = null;
		
		// case 1 - filter by id
		if (id != null) {
			log.debug("search for id {}", id);
			List<Long> ids = new ArrayList<>();
			ids.add(id);
			page = repository.findAllById(ids, pageRequest);
		}
		// case 2 - filter by creationDate
		else if (created != null) {
			log.debug("search for created {}", created.toString());

			SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String createdString = inputFormat.format(created);
			String beginString = createdString + " 00:00:00";
			String endString = createdString + " 23:59:59";
			Date dayBegin = created;
			Date dayEnd = created;
			try {
				dayBegin = outputFormat.parse(beginString);
				dayEnd = outputFormat.parse(endString);
			} catch (ParseException e) {
				log.error(e.getMessage());
			}

			log.debug("search orders created between {} and {}", dayBegin, dayEnd);
			page = repository.findByCreatedBetween(dayBegin, dayEnd, pageRequest);
		}
		// case 3 - filter by status
		else if (status != null && status.trim().length() > 0) {
			log.debug("search for status {}", status.trim());
			page = repository.findByOrderStatus(status.trim(), pageRequest);
		}
		// case 4 - filter by user assignment
		else if (assigned != null && assigned.trim().length() > 0) {
			String assignmentString = assigned.trim().toLowerCase();
			String userNameString = "";

			log.debug("search for assigned {}", assignmentString);

			boolean usernameGiven = false;
			if (username != null && username.trim().length() > 0) {
				log.debug("username {}", username);
				usernameGiven = true;
				userNameString = username.trim();
			}

			if (assignmentString.equals("nobody")) {
				log.debug("search orders not yet assigned");
				page = repository.findByUserIsNull(pageRequest);
			} else if (usernameGiven && assignmentString.equals("own")) {
				log.debug("search order assigned to {}", userNameString);
				page = repository.findByUserUsername(userNameString, pageRequest);
			} else if (usernameGiven && assignmentString.endsWith("others")) {
				log.debug("search order not assigned to {}", userNameString);
				page = repository.findByUserUsernameNot(userNameString, pageRequest);
			} else {
				log.debug(" get all orders");
				page = repository.findAll(pageRequest);
			}
		}
		// case 5 - no filter
		else {
			log.debug("no filter - get all locations");
			page = repository.findAll(pageRequest);
		}

		return page;
	}
	
	/*
	 * POST a new picking order
	 */
	@PostMapping("/orders")
	public PickingOrder newPickingOrder(@RequestBody PickingOrder newPickingOrder) {
		return repository.save(newPickingOrder);
	}
	
	/* 
	 * GET a single picking order
	 */
	@GetMapping("/orders/{id}")
	public PickingOrder getPickingOrderById(@PathVariable Long id) {
		return repository.findById(id).orElseThrow(() -> new PickingOrderNotFoundException(id));
	}
	
	/*
	 * PUT an existing picking order
	 */
	@PutMapping("/orders/{id}")
	public PickingOrder updatePickingOrder(@RequestBody PickingOrder updatedOrder, @PathVariable Long id) {
		Optional<PickingOrder> orderOptional = repository.findById(id);
		if(orderOptional.isPresent()) {
			PickingOrder order = orderOptional.get();
			order.setLastChanged(updatedOrder.getLastChanged());
			order.setOrderName(updatedOrder.getOrderName());
			order.setOrderStatus(updatedOrder.getOrderStatus());
			order.setUser(updatedOrder.getUser());
			return repository.save(order);
		}
		else {
			throw new PickingOrderNotFoundException(id);
		}
	}
	
	/*
	 * DELETE an existing picking order
	 */
	@DeleteMapping("/orders/{id}")
	public void deletePickingOrder(@PathVariable Long id) {
		repository.deleteById(id);
	}
	
	/*
	 * GET list of picking items
	 * for a single picking order
	 */
	@GetMapping("/orders/{id}/items")
	public List<PickingItem> getItemListForOrder(@PathVariable Long id) {
		PickingOrder order = repository.findById(id)
				.orElseThrow(() -> new PickingOrderNotFoundException(id));
		List<PickingItem> itemList = new ArrayList<>();
		if(order != null) {
			itemList = order.getItems();
		}
		return itemList;
	}
	
	/*
	 * POST a new picking item
	 * for a single picking order
	 */
	@PostMapping("/orders/{orderId}/items")
	public PickingItem addItemToOrder(@PathVariable Long orderId, @RequestBody PickingItem newItem) {
		log.debug("addItemToOrder - orderId = {}", orderId);

		if(newItem.getOrder() == null) {
			log.debug("item has no order?");
			PickingOrder order = repository.findById(orderId)
					.orElseThrow(() -> new PickingOrderNotFoundException(orderId));
			newItem.setOrder(order);
		}
		
		return itemRepository.save(newItem);
	}
	
}
