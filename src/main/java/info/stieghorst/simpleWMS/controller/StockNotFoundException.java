package info.stieghorst.simpleWMS.controller;

/*
 * exception: stock not found
 */
public class StockNotFoundException extends RuntimeException {
	
	StockNotFoundException(Long id) {
		super("Could not find stock with id: " + id);
	}

}
