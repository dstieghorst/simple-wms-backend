package info.stieghorst.simpleWMS.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.domain.User;
import info.stieghorst.simpleWMS.repository.RoleRepository;
import info.stieghorst.simpleWMS.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;

/*
 * REST controller for all user related requests
 */
@RestController
@Slf4j
public class UserController {
	
	private final UserRepository repository;
	private final RoleRepository roleRepository;
	
	public UserController(UserRepository repository, RoleRepository roleRepository) {
		this.repository = repository;
		this.roleRepository = roleRepository;
	}
	
	/*
	 * GET list of users
	 * search criteria not required
	 */
	@GetMapping("/users")
	List<User> findUsers(@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "roleName", required = false) String roleName) {
		log.debug("UserController.findUsers - START");
		
		List<User> emptyList = new ArrayList<>();
		
		if(username != null && username.trim().length() > 0) {
			log.debug("UserController.findUsers - username {}", username);
			List<User> users = new ArrayList<>();
			users.add(repository.findByUsername(username));
			return users;
		}
		else if(roleName != null && roleName.trim().length() > 0) {
			log.debug("UserController.findUsers - roleName {}", roleName);
			Role role = roleRepository.findByRoleName(roleName);
			if(role != null) {
				return repository.findByRole(role);
			}
			else {
				return emptyList;
			}
		}
		else {
			log.debug("UserController.findUsers - no filter");
			return repository.findAll();
		}
		
	}
	
	/* 
	 * POST a new user
	 */
	@PostMapping("/users")
	User newUser(@RequestBody User newUser) {
		log.debug("UserController.newUser - START");
		log.debug("UserController.newUser - newUser {}", newUser);
		return repository.save(newUser);
	}
	
	/*
	 * GET a single user
	 */
	@GetMapping("/users/{id}")
	User getUserById(@PathVariable Long id) {
		log.debug("UserController.getUserById - START - id {}", id);
		return repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
	}
	
	/*
	 * PUT an existing user
	 */
	@PutMapping("/users/{id}")
	User updateUser(@RequestBody User updatedUser, @PathVariable Long id) {
		log.debug("UserController.updatedUser - START - id {}", id);
		log.debug("UserController.updatedUser - updatedUser {}", updatedUser);
		
		Optional<User> userOptional = repository.findById(id);
		if(userOptional.isPresent()) {
			User user = userOptional.get();
			log.debug("UserController.updatedUser - existing user {}", user);
			user.setPassword(updatedUser.getPassword());
			return repository.save(user);
		}
		else {
			throw new UserNotFoundException(id);
		}
	}
	
	/*
	 * DELETE an existing user
	 */
	@DeleteMapping("/users/{id}")
	void deleteUser(@PathVariable Long id) {
		log.debug("UserController.deleteUser - START - id {}", id);
		repository.deleteById(id);
	}

	/*
	 * GET role list for a single user,
	 * identified by the user id
	 */
	@GetMapping("/users/{id}/roles")
	Set<Role> getRolesForUserById(@PathVariable Long id) {
		log.debug("UserController.getRolesForUserById - START - id {}", id);
		
		Optional<User> userOptional = repository.findById(id);
		if(userOptional.isPresent()) {
			return userOptional.get().getRoles();
		}
		else {
			throw new UserNotFoundException(id);
		}
	}
	
	/*
	 * PUT an existing role for an existing user
	 * identified by user ID/role ID
	 */
	@PutMapping("/users/{userId}/roles/{roleId}")
	User addRoleForUser(@PathVariable Long userId, @PathVariable Long roleId) {
		log.debug("UserController.addRoleForUser - START - userId {}, roleId {}", userId, roleId);
		
		Optional<User> userOptional = repository.findById(userId);
		if(userOptional.isPresent()) {
			User user = userOptional.get();
			Role role = roleRepository.findById(roleId).orElse(null);
			if(role != null) {
				Predicate<Role> rolePredicate = r -> r.getId().equals(roleId);
				user.getRoles().removeIf(rolePredicate);
				user.addRole(role);
				return repository.save(user);
			}
			else {
				return user;
			}
		}
		else {
			throw new UserNotFoundException(userId);
		}
	}
	
	/*
	 * DELETE role for a single user
	 */
	@DeleteMapping("/users/{userId}/roles/{roleId}")
	User deleteRoleForUser(@PathVariable Long userId, @PathVariable Long roleId) {
		log.debug("UserController.deleteRoleForUser - START - userId {}, roleId {}", userId, roleId);
		
		Optional<User> userOptional = repository.findById(userId);
		if(userOptional.isPresent()) {
			User user = userOptional.get();
			Predicate<Role> rolePredicate = r -> r.getId().equals(roleId);
			user.getRoles().removeIf(rolePredicate);
			return repository.save(user);
		}
		else {
			throw new UserNotFoundException(userId);
		}
		
	}
	
}
