package info.stieghorst.simpleWMS.controller;

/*
 * exception: picking item not found
 */
public class PickingItemNotFoundException extends RuntimeException {
	
	PickingItemNotFoundException(Long id) {
		super("Could not find stock with id: " + id);
	}

}
