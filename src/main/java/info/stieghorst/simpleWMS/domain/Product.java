package info.stieghorst.simpleWMS.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.ToString;

/*
 * stock keeping unit
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
@Entity
@ToString(exclude="stocks")
public class Product {

	// fields

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique=true)
	private String productNo;
	
	private String name;
	
	private String description;
	
	private boolean active;
	
	@OneToMany(mappedBy="product", fetch=FetchType.EAGER)
	@JsonIgnore
	private List <Stock> stocks = new ArrayList<>();

	// constructors
	
	public Product() {
		
	}

	public Product(String productNo, String name, String description, boolean active) {
		this.productNo = productNo;
		this.name = name;
		this.description = description;
		this.active = active;
	}
	
	

}
