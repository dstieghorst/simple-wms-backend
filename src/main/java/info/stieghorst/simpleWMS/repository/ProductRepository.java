package info.stieghorst.simpleWMS.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import info.stieghorst.simpleWMS.domain.Product;

/*
 * make use of Spring PagingAndSortingRepository for product CRUD operations, with pagination option
 */
public interface ProductRepository extends PagingAndSortingRepository<Product, Long>{
	
	// id
	
	Page<Product> findAllById(List<Long> ids, Pageable pageable);

	// productNo
	
	List<Product> findByProductNoIgnoreCaseContaining(String productNo);
	
	Page<Product> findByProductNoIgnoreCaseContaining(String productNo, Pageable pageable);
	
	// name

	List<Product> findByNameIgnoreCaseContaining(String name);
	
	Page<Product> findByNameIgnoreCaseContaining(String name, Pageable pageable);
	
	// description

	List<Product> findByDescriptionIgnoreCaseContaining(String description);

	Page<Product> findByDescriptionIgnoreCaseContaining(String description, Pageable pageable);
	
	// activ
	
	List<Product> findDistinctByActiveAndStocksIsNotNull(boolean active);
	
	Page<Product> findDistinctByActiveAndStocksIsNotNull(boolean active, Pageable pageable);

}
