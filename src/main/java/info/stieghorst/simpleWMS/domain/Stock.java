package info.stieghorst.simpleWMS.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Data;

/*
 * stock: product - location - units
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
@Entity
public class Stock {

	// fields

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch=FetchType.EAGER)
	private Product product;

	@OneToOne(fetch=FetchType.EAGER)
	private Location loc;

	private Integer units;

	// constructors
	
	public Stock() {
	}

	public Stock(Product product, Location loc, Integer units) {
		this.product = product;
		this.loc = loc;
		this.units = units;
	}

}
