package info.stieghorst.simpleWMS.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import info.stieghorst.simpleWMS.domain.PickingItem;

/*
 * make use of Spring JpaRepository for picking item related CRUD operations
 */
public interface PickingItemRepository extends JpaRepository<PickingItem, Long>{
	
	List<PickingItem> findByOrderId(Long id, Sort sort);
	
}
