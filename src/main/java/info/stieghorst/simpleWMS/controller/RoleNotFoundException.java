package info.stieghorst.simpleWMS.controller;

/*
 * exception: role not found
 */
public class RoleNotFoundException extends RuntimeException {
	
	RoleNotFoundException(Long id) {
		super("Could not find role with id: " + id);
	}

}
