package info.stieghorst.simpleWMS.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/*
 * storage location
 * 3 part coordinates
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
@Entity
public class Location {

	// fields

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer aisle;
	
	private Integer stack;
	
	private Integer level;
	
	private Boolean locked;
	
	@OneToOne(mappedBy="loc")
	@JsonIgnore
	private Stock stock;

	// constructors
	
	public Location() {
		
	}

	public Location(Integer aisle, Integer stack, Integer level) {
		this.aisle = aisle;
		this.stack = stack;
		this.level = level;
		this.locked = false; // by default not locked
	}
	
}
