package info.stieghorst.simpleWMS.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import info.stieghorst.simpleWMS.domain.Location;

/*
 * make use of Spring PagingAndSortingRepository for location CRUD operations with pagination option
 */
public interface LocationRepository extends PagingAndSortingRepository<Location, Long>{
	
	// id
	
	Page<Location> findAllById(Long id, Pageable pageable);
	
	// lockFlag

	List<Location> findByLocked(Boolean locked, Sort sort);

	Page<Location> findByLocked(boolean locked, Pageable pageable);
	
	// stock == null

	@Query(value = "select * from location l " 
			+ "where not exists (select 1 from stock s where s.loc_id = l.id) "
			+ " order by aisle, stack, level", 
			nativeQuery = true)
	List<Location> findByStockIsNull();
	
	@Query(value = "select * from location l "
			+ "where not exists (select 1 from stock s where s.loc_id = l.id)", 
			nativeQuery = true)
	Page<Location> findByStockIsNull(Pageable pageable);
	
	// locked and stock == null

	@Query(value = "select * from location l " 
			+ " where locked = ?1 "
			+ " and not exists (select 1 from stock s where s.loc_id = l.id) "
			+ " order by aisle, stack, level", 
			nativeQuery = true)
	List<Location> findByLockedAndStockIsNull(Boolean locked);
	
	@Query(value = "select * from location l " 
			+ " where locked = ?1 "
			+ " and not exists (select 1 from stock s where s.loc_id = l.id) ", 
			nativeQuery = true)
	Page<Location> findByLockedAndStockIsNull(Boolean locked, Pageable pageable);
	
	// aisle
	
	List<Location> findByAisle(Integer aisle, Sort sort);
	
	Page<Location> findByAisle(Integer aisle, Pageable pageable);
	
	// aisle and stack

	List<Location> findByAisleAndStack(Integer aisle, Integer stack, Sort sort);
	
	Page<Location> findByAisleAndStack(Integer aisle, Integer stack, Pageable pageable);

}
