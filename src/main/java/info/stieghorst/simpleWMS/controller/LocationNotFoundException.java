package info.stieghorst.simpleWMS.controller;

/*
 * exception: location not found
 */
public class LocationNotFoundException extends RuntimeException {
	
	LocationNotFoundException(Long id) {
		super("Could not find location with id: " + id);
	}

}
