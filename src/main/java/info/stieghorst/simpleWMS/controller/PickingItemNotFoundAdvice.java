package info.stieghorst.simpleWMS.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/* 
 * controller advice class for picking item controller
 */
@ControllerAdvice
public class PickingItemNotFoundAdvice {
	
	@ResponseBody
	@ExceptionHandler(PickingItemNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String stockNotFoundHandler(PickingItemNotFoundException ex) {
		return ex.getMessage();
	}

}
