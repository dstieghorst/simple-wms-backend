package info.stieghorst.simpleWMS.controller;

/*
 * exception: user not found
 */
public class UserNotFoundException extends RuntimeException {
	
	UserNotFoundException(Long id) {
		super("Could not find user with id: " + id);
	}

}
