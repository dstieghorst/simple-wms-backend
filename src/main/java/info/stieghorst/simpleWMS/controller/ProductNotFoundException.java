package info.stieghorst.simpleWMS.controller;

/*
 * exception: product not found
 */
public class ProductNotFoundException extends RuntimeException {
	
	ProductNotFoundException(Long id) {
		super("Could not find product with id: " + id);
	}

}
