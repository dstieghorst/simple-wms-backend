package info.stieghorst.simpleWMS.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/* 
 * controller advice class for location controller
 */
@ControllerAdvice
public class LocationNotFoundAdvice {
	
	@ResponseBody
	@ExceptionHandler(LocationNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String locationNotFoundHandler(LocationNotFoundException ex) {
		return "EXCEPTION!!! - " + ex.getMessage();
	}

}
