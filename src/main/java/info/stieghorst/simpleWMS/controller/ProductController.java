package info.stieghorst.simpleWMS.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.stieghorst.simpleWMS.domain.Product;
import info.stieghorst.simpleWMS.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;

/*
 * REST controller for all product related requests
 */
@RestController
@Slf4j
public class ProductController {
	
	private final ProductRepository repository;
	
	private final Integer DEFAULT_PAGE_SIZE = 5;

	public ProductController(ProductRepository repository) {
		this.repository = repository;
	}
	
	/*
	 * GET list of products
	 * search criteria not required
	 */
	@GetMapping("/products")
	List<Product> getProducts(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "productNo", required = false) String productNo,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "available", required = false) Boolean available
	) {
		log.debug("getProducts");
		
		List<Product> productList = new ArrayList<>();
		
		// case 1 - filter by id
		if (id != null) { 
			log.debug("search for id {}", id);
			List<Long> ids = new ArrayList<>();
			ids.add(id);
			repository.findAllById(ids).forEach(productList::add);
		}
		// case 2 - filter by productNo
		else if(productNo != null && productNo.trim().length() > 0) {
			productList = repository.findByProductNoIgnoreCaseContaining(productNo);
		}
		// case 3 - filter by productName
		else if(name != null && name.trim().length() > 0) {
			productList = repository.findByNameIgnoreCaseContaining(name);
		}
		// case 4 - filter by description
		else if(description != null && description.trim().length() > 0) {
			productList = repository.findByDescriptionIgnoreCaseContaining(description);
		}
		// case 5 - only active products with stock
		else if(available != null && available == true) {
			productList = repository.findDistinctByActiveAndStocksIsNotNull(true);
		}
		// case 6 - no filter
		else {
			repository.findAll().forEach(productList::add);
		}

		return productList;
	}
	
	/*
	 * GET list of products
	 * search criteria not required
	 * 
	 * - version for access by page
	 *   prefix "/paged" in URL
	 */
	@GetMapping("/paged/products")
	Page<Product> getProductsPage(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "productNo", required = false) String productNo,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "available", required = false) Boolean available,
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize
	) {
		log.debug("getProductsPage");
		
		PageRequest pageRequest = null;
		if(pageNumber == null) {
			pageNumber = 0;
		}
		if (pageSize == null || pageSize < 0) {
			pageSize = DEFAULT_PAGE_SIZE;
		}
		pageRequest = PageRequest.of(pageNumber, pageSize);
		log.debug("pageNumber {}, pageSize {}", pageNumber, pageSize);

		Page<Product> page = null;
		
		// case 1 - filter by id, with pagination
		if (id != null) { 
			log.debug("search for id {}", id);
			List<Long> ids = new ArrayList<>();
			ids.add(id);
			page = repository.findAllById(ids, pageRequest);
		}
		// case 2 - filter by productNo, with pagination
		else if(productNo != null && productNo.trim().length() > 0) {
			page = repository.findByProductNoIgnoreCaseContaining(productNo, pageRequest);
		}
		// case 3 - filter by productName, with pagination
		else if(name != null && name.trim().length() > 0) {
			page = repository.findByNameIgnoreCaseContaining(name, pageRequest);
		}
		// case 4 - filter by description, with pagination
		else if(description != null && description.trim().length() > 0) {
			page = repository.findByDescriptionIgnoreCaseContaining(description, pageRequest);
		}
		// case 5 - only active products with stock - only used without pagination
		else if(available != null && available == true) {
			page = repository.findDistinctByActiveAndStocksIsNotNull(true, pageRequest);
		}
		// case 6 - no filter, with pagination
		else {
			page = repository.findAll(pageRequest);
		}

		return page;
	}
	
	/*
	 * POST a new product
	 */
	@PostMapping("/products")
	Product newProduct(@RequestBody Product newProduct) {
		return repository.save(newProduct);
	}
	
	/*
	 * GET a single product
	 */
	@GetMapping("/products/{id}")
	Product getProductById(@PathVariable Long id) {
		return repository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
	}
	
	/*
	 * PUT an existing product
	 */
	@PutMapping("/products/{id}")
	Product updateProduct(@RequestBody Product updatedProduct, @PathVariable Long id) {
		Optional<Product> productOptional = repository.findById(id);
		if(productOptional.isPresent()) {
			Product product = productOptional.get();
			product.setActive(updatedProduct.isActive());
			product.setDescription(updatedProduct.getDescription());
			product.setName(updatedProduct.getName());
			product.setProductNo(updatedProduct.getProductNo());
			return repository.save(product);
		}
		else {
			throw new ProductNotFoundException(id);
		}
	}
	
	/*
	 * DELETE a product
	 */
	@DeleteMapping("/products/{id}")
	void deleteProduct(@PathVariable Long id) {
		repository.deleteById(id);
	}

}
