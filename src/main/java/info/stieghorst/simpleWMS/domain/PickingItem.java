package info.stieghorst.simpleWMS.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/*
 * picking item
 * 
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
@Entity
@Table(name = "picking_item")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PickingItem {
	
	public static final String STATUS_OPEN = "OFFEN";
	public static final String STATUS_FINISHED = "ERLEDIGT";
	
	// fields

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JsonIgnore
	private PickingOrder order;
	
	@OneToOne
	private Product product;
	
	private Integer orderQuantity;
	
	private Integer actualQuantity;
	
	private String itemStatus;
	
	public PickingItem() {
		actualQuantity = 0;
		itemStatus = STATUS_OPEN;
	}

	public PickingItem(PickingOrder order, Product product, Integer orderQuantity) {
		this.order = order;
		this.product = product;
		this.orderQuantity = orderQuantity;
		this.actualQuantity = 0;
		this.itemStatus = STATUS_OPEN;
	}
	
	

}
