package info.stieghorst.simpleWMS.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import info.stieghorst.simpleWMS.domain.Stock;
import info.stieghorst.simpleWMS.repository.StockRepository;
import lombok.extern.slf4j.Slf4j;

/*
 * REST controller for all stock related requests
 */
@RestController
@Slf4j
public class StockController {
	
	private final StockRepository repository;
	
	private final Integer DEFAULT_PAGE_SIZE = 5;

	private Sort standardSort = new Sort(Sort.Direction.ASC, "product.productNo") 
			.and(new Sort(Sort.Direction.ASC, "loc.aisle"))
			.and(new Sort(Sort.Direction.ASC, "loc.stack"))
			.and(new Sort(Sort.Direction.ASC, "loc.level"));
	
	public StockController(StockRepository repository) {
		this.repository = repository;
	}
	
	
	/*
	 * GET stock list
	 * search criteria not required
	 */
	@GetMapping("/stocks")
	List<Stock> getStocks(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "productId", required = false) Long productId,
			@RequestParam(value = "productNo", required = false) String productNo,
			@RequestParam(value = "locationId", required = false) Long locationId,
			@RequestParam(value = "noLocation", required = false) Boolean noLocation,
			@RequestParam(value = "aisle", required = false) Integer aisle,
			@RequestParam(value = "stack", required = false) Integer stack,
			@RequestParam(value = "level", required = false) Integer level
			) {
		
		log.debug("getStocks");
		
		List<Stock> stockList = new ArrayList<>();
		
		// case 1 - filter by id
		if (id != null) { 
			log.debug("search for id {}", id);
			List<Long> ids = new ArrayList<>();
			ids.add(id);
			repository.findAllById(ids).forEach(stockList::add);
		}
		// case 2 - filter by productId
		else if(productId != null) {
			log.debug("search for productId {}", productId);
			stockList = repository.findByProductId(productId, standardSort);
		}
		// case 3 - filter by locationId
		else if(locationId != null) {
			log.debug("search for locationId {}", locationId);
			stockList = repository.findByLocId(locationId, standardSort);
		}
		// case 4 - filter by productNo
		else if(productNo != null && productNo.trim().length() > 0) {
			log.debug("search for productNo {}", productNo.trim());
			stockList = repository.findByProductProductNoContaining(productNo.trim(), standardSort);
		}
		// case 5 - stock without location
		else if(noLocation != null && noLocation == true) {
			stockList = repository.findByLocIsNull(standardSort);
		}
		// case 6 - filter by aisle
		else if(aisle != null && aisle > 0 
				&& (stack == null || stack == 0)) {
			stockList = repository.findByLocAisle(aisle, standardSort);
		}
		// case 7 - filter by aisle and stack
		else if(aisle != null && aisle > 0 
				&& stack != null && stack > 0
				&&( level == null || level == 0)) {
			stockList = repository.findByLocAisleAndLocStack(aisle, stack, standardSort);
		}
		// case 8 - filter by exact location
		else if(aisle != null && aisle > 0 
				&& stack != null && stack > 0
				&& level != null && level > 0) {
			stockList = repository.findByLocAisleAndLocStackAndLocLevel(aisle, stack, level, standardSort);
			
		}
		// case 9 - no filter
		else {
			log.debug("no filter - get all locations");
			repository.findAll().iterator().forEachRemaining(stockList::add);
		}
		
		return stockList;
	}
	
	/*
	 * GET stock list
	 * search criteria not required
	 * 
	 * - version for access by page
	 *   prefix "/paged" in URL
	 */
	@GetMapping("/paged/stocks")
	Page<Stock> getStocksPage(
			@RequestParam(value = "id", required = false) Long id,
			@RequestParam(value = "productId", required = false) Long productId,
			@RequestParam(value = "productNo", required = false) String productNo,
			@RequestParam(value = "locationId", required = false) Long locationId,
			@RequestParam(value = "noLocation", required = false) Boolean noLocation,
			@RequestParam(value = "aisle", required = false) Integer aisle,
			@RequestParam(value = "stack", required = false) Integer stack,
			@RequestParam(value = "level", required = false) Integer level,
			@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize
			) {
		
		log.debug("getStocksPage");
		
		PageRequest pageRequest = null;
		if(pageNumber == null) {
			pageNumber = 0;
		}
		if (pageSize == null || pageSize < 0) {
			pageSize = DEFAULT_PAGE_SIZE;
		}
		pageRequest = PageRequest.of(pageNumber, pageSize);
		log.debug("pageNumber {}, pageSize {}", pageNumber, pageSize);


		Page<Stock> page = null;
		
		// case 1 - filter by id
		if (id != null) { 
			log.debug("search for id {}", id);
			List<Long> ids = new ArrayList<>();
			ids.add(id);
			page = repository.findAllById(ids, pageRequest);
		}
		// case 2 - filter by productId
		else if(productId != null) {
			log.debug("search for productId {}", productId);
			page = repository.findByProductId(productId, pageRequest);
		}
		// case 3 - filter by locationId
		else if(locationId != null) {
			log.debug("search for locationsId {}", locationId);
			page = repository.findByLocId(locationId, pageRequest);
		}
		// case 4 - filter by productNo
		else if(productNo != null && productNo.trim().length() > 0) {
			log.debug("search for productNo {}", productNo.trim());
			page = repository.findByProductProductNoContaining(productNo, pageRequest);
		}
		// case 5 - stock without location
		else if(noLocation != null && noLocation == true) {
			page = repository.findByLocIsNull(pageRequest);
		}
		// case 6 - filter by aisle
		else if(aisle != null && aisle > 0 
				&& (stack == null || stack == 0)) {
			page = repository.findByLocAisle(aisle, pageRequest);
		}
		// case 7 - filter by aisle and stack
		else if(aisle != null && aisle > 0 
				&& stack != null && stack > 0
				&&( level == null || level == 0)) {
			page = repository.findByLocAisleAndLocStack(aisle, stack, pageRequest);
		}
		// case 8 - filter by exact location
		else if(aisle != null && aisle > 0 
				&& stack != null && stack > 0
				&& level != null && level > 0) {
			page = repository.findByLocAisleAndLocStackAndLocLevel(aisle, stack, level, pageRequest);
		}
		// case 9 - no filter
		else {
			log.debug("no filter - get all stocks");
			page = repository.findAll(pageRequest);
		}
			
		return page;
	}
	
	/*
	 * POST a new stock
	 */
	@PostMapping("/stocks")
	Stock newStock(@RequestBody Stock newStock) {
		return repository.save(newStock);
	}
	
	/*
	 * GET a single stock
	 */
	@GetMapping("/stocks/{id}")
	Stock getStockById(@PathVariable Long id) {
		return repository.findById(id).orElseThrow(() -> new StockNotFoundException(id));
	}
	
	/*
	 * PUT an existing stock
	 */
	@PutMapping("/stocks/{id}")
	Stock updateStock(@RequestBody Stock updatedStock, @PathVariable Long id) {
		Optional<Stock> stockOptional = repository.findById(id);
		if(stockOptional.isPresent()) {
			Stock stock = stockOptional.get();
			stock.setLoc(updatedStock.getLoc());
			stock.setUnits(updatedStock.getUnits());
			return repository.save(stock);
		}
		else {
			throw new StockNotFoundException(id);
		}
	}
	
	/*
	 * delete an existing stock
	 */
	@DeleteMapping("/stocks/{id}")
	void deleteStock(@PathVariable Long id) {
		repository.deleteById(id);
	}

}
